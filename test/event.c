/*
 * Lua plugin for the GNU Compiler Collection.
 * Copyright © 2012 Peter Colberg.
 * For conditions of distribution and use, see copyright notice in LICENSE.
 */

typedef union {
  int up;
  int charm;
  int top;
} up_charm_top_quarks;

void down_strange_bottom_quarks(void)
{
  int __attribute__((unused)) down;
  int __attribute__((unused)) strange;
  int __attribute__((unused)) bottom;
}
